﻿import React, { Component, Fragment } from 'react';
import './Main.css';
import axios from 'axios';

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchQuery: '',
            cityId: [],
            result: [],
            AvgTemp: '',
            YesterdayDate: new Date(new Date().setDate(new Date().getDate()-1)).toISOString().slice(0, 10).replace(/-/g, "/"),
            isLoaded: false,
            MaxTemp: '',
            MinTemp: '',
            WindSpeed: '',
            onLoad: false,
    };
        this.handleChange = this.handleChange.bind(this);
        this.fetchRequest = this.fetchRequest.bind(this);
        this.sendRequest = this.sendRequest.bind(this);
    }


    handleChange(val) {
        this.setState({ [val.target.name]: val.target.value });
    }

    sendRequest({ key }) {
        if (key == 'Enter') {
            this.setState({ onLoad: true, isLoaded: false });
            this.fetchRequest();
            }
        }

    fetchRequest() {
        const CORS = 'https://cors-anywhere.herokuapp.com/';
        const queryString = 'query=' + this.state.searchQuery;
        const entryURL = 'https://www.metaweather.com/api/location/search/?';
        const UrlWithInput = `${CORS}${entryURL}${queryString}`;
        const RequestURL = 'https://www.metaweather.com/api/location/';

        //First request, send input and get city ID 
        axios.get(UrlWithInput)
            .then(response => {
                const cityId = response.data["0"].woeid;
                this.setState({ cityId });
            })

            //Second chained request, sent city ID and yesterday's date to get weather info
            .then(response => {
                return axios.get(CORS + RequestURL + this.state.cityId + "/" + this.state.YesterdayDate)
            })
            .then(res => {
                const result = res.data["0"];
                this.setState({
                    isLoaded: true,
                    result: result,
                    MaxTemp: Math.floor(result.max_temp),
                    MinTemp: Math.floor(result.min_temp),
                    WindSpeed: Math.floor(result.wind_speed * 1.6),
                    AvgTemp: Math.floor((result.max_temp + result.min_temp) / 2),
                });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        const { result, AvgTemp, YesterdayDate, isLoaded, MaxTemp, MinTemp, WindSpeed, onLoad } = this.state;
        return (
            <Fragment>
                <div>
                    <div id="box">
                        <input type="text" id="mySearch" name="searchQuery" onChange={this.handleChange} onKeyPress={this.sendRequest} placeholder="" autofocus="true" />
                    </div>
                    <div>
                        {!onLoad ? (<div></div>) : (isLoaded ? 
                            (<ul id = "list" >
                                <li> Yesterday ({YesterdayDate}) it was mostly {result.weather_state_name}
                                    {(() => {
                                        switch (result.weather_state_name) {
                                            case "Showers": return <img src="https://www.metaweather.com/static/img/weather/s.svg" width="50" height="50" />;
                                            case "Snow": return <img src="https://www.metaweather.com/static/img/weather/sn.svg" width="60" height="60" />;
                                            case "Sleet": return <img src="https://www.metaweather.com/static/img/weather/sl.svg" width="60" height="60" />;
                                            case "Hail": return <img src="https://www.metaweather.com/static/img/weather/h.svg" width="60" height="60" />;
                                            case "Thunderstorm": return <img src="https://www.metaweather.com/static/img/weather/t.svg" width="60" height="60" />;
                                            case "Heavy Rain": return <img src="https://www.metaweather.com/static/img/weather/hr.svg" width="60" height="60" />;
                                            case "Light Rain": return <img src="https://www.metaweather.com/static/img/weather/lr.svg" width="60" height="60" />;
                                            case "Heavy Cloud": return <img src="https://www.metaweather.com/static/img/weather/hc.svg" width="60" height="60" />;
                                            case "Light Cloud": return <img src="https://www.metaweather.com/static/img/weather/lc.svg" width="60" height="60" />;
                                            case "Clear": return <img src="https://www.metaweather.com/static/img/weather/c.svg" width="60" height="60" />;
                                            default: return;
                                        }
                                    })()}
                                </li>
                                <li>With average temperature of {AvgTemp}C (Min: {MinTemp}C and Max: {MaxTemp}C ) </li>
                                <li> And wind speed of {WindSpeed}km/h "{result.wind_direction_compass}" direction. </li>
                            </ul>
                            ) : (
                            <h2 id="Loading">Loading...</h2>)
                        )}              
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default Main;